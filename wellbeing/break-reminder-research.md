Break reminder research
===

Some very basic research done by @pwithnall around what people would like from break reminder functionality in GNOME Shell.

In particular, I was interested in working out what break interval/durations to use, and how people would like to be notified that it’s break time.

Prior art
---

 - https://flathub.org/apps/org.gnome.BreakTimer
   * ‘restbreak’s and ‘microbreak’s for long/infrequent and short/frequent breaks
   * Notifications
   * Always on
   * Allows a break to be skipped
 - https://breaktimer.app/
   * Only one duration/interval of break
   * Notifications
   * Allows break reminders only during working hours
   * Allows a break to be skipped
 - http://www.workrave.org/
   * ‘restbreak’s and ‘microbreak’s for long/infrequent and short/frequent breaks
   * Notifications
   * Always on
   * Allows a break to be skipped
   * Supports a daily use limit
 - https://github.com/hovancik/stretchly/
   * ‘break’s and ‘microbreak’s for long/infrequent and short/frequent breaks
   * Notifications
   * Colour scheme and sound themes
   * Allows a break to be skipped

Polling
---

Source: https://mastodon.social/@pwithnall/112020286750748527

Question 1:
> Quick poll for everyone (not just techie types). If you use a break reminder app on your computer, what settings do you use with it? Select all that apply (e.g. if you use a mix of long and short breaks, select both!)
>
> I’d be interested in replies saying what break lengths and frequencies work for people, too. The numbers in the poll options are only ballpark.

Results (24 responses):
 1. 92% Infrequent, long breaks (e.g 5 min every hour)
 2. 4% Frequent, short breaks (e.g. 30s every 5 minutes)
 3. 8% Something else (please reply)

Question 2:
> Second to that, when it’s time to take a break, what do you prefer? Do you want your break reminder app to trust you to take a break when notified, or to force you to?

Results (8 responses):
 1. 25% One unobtrusive notification, no reminders
 2. 50% One notification, with reminders until you stop
 3. 13% Computer screen is forcefully locked during break
 4. 13% Other (please reply!)

The replies to both questions mentioned:
 - “Full screen lock but with a skip button (incase I'm in a meeting or something)”
 - “it needs to lock to make the break happen, but this is also super stressful/triggering, so it needs quite regular/visible reminders about approaching breaks / end times. 5 mins remaining, 2 mins remaining, then a persistently visible 60 second couontdown or something”
 - “30s every 20 minutes”

Official guidance
---

A non-exhaustive search for official guidance on breaks from computers came up with 
[guidance from the UK Health and Safety Executive](https://www.hse.gov.uk/pubns/indg36.pdf).

It recommends short, frequent breaks rather than long, infrequent ones.
