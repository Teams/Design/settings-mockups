# GNOME Settings Mockups

This repository contains mockups and concepts for GNOME Settings application.

Design pages can be found on the wiki for [individual panels](https://wiki.gnome.org/Design/SystemSettings/) as well as the [common parts of the app](https://wiki.gnome.org/Design/Apps/Settings/).

Old mockups can be found in the [archived GitHub repository](https://github.com/gnome-design-team/gnome-mockups/tree/master/system-settings).

# For Developers

**Design materials in this repository should be considered non-final or experimental.** Mockups should not implemented as-seen unless they are presented directly, via a ticket against a particular project or other means, for implementation. If you are looking to implement some of these concepts for a GNOME module reach out to the [Design Team](https://gitlab.gnome.org/Teams/Design/) about interest in doing so we can be of assistance.

# Other Mockups

The following repositories contain other GNOME mockups:
- [App Mockups](https://gitlab.gnome.org/Teams/Design/app-mockups) (this repo): Core apps, if they're not too complex
- [Settings Mockups](https://gitlab.gnome.org/Teams/Design/settings-mockups)
- [Software Mockups](https://gitlab.gnome.org/Teams/Design/software-mockups)
- [Other App Mockups](https://gitlab.gnome.org/Teams/Design/other-app-mockups): Non-core apps
- [OS Mockups](https://gitlab.gnome.org/Teams/Design/os-mockups): Shell, design patterns, and other system level stuff
